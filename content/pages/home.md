Title: Design & Libre
Date: 2020-04-25
Category: Python
Authors: Design & Libre
Url:
Save_as: index.html


Design & Libre est un groupe d'individus qui souhaite mettre en relation des designers qui veulent contribuer au libre et les projets ou les communautés développant ces projets :)



## Où nous trouver :



Framateam : [https://framateam.org/ux-framatrucs/](https://framateam.org/ux-framatrucs/)

Compte Mastodon :  [https://mastodon.xyz/@design\_libre](https://mastodon.xyz/@design\_libre)



## FAQs



### FAQ pour les designers qui veulent contribuer à du logiciel libre



   * **Comment je trouve des projets à aider ?**
       * Tu peux trouver des demandes de contribution sur notre mastodon : [@design\_libre](https://mastodon.xyz/@design\_libre).
       * Sur [Framacolibri](https://framacolibri.org/c/graphistes/6), des demandes sont aussi régulièrement postées dans la rubrique "Graphisme".
       * Sinon, passe sur [Framateam](https://framateam.org/ux-framatrucs/channels/town-square) pour qu'on en discute ensemble !
   * **Comment faire pour que ça se passe au mieux ?**
       * Expliquer sur Framateam ce que tu as envie de faire sur des projets  (retravailler une interface / un parcours utilisateur, faire des tests utilisateur, créer une charte graphique...).
       * Interroger l'équipe pour comprendre la place qu'ils attendent du designer et s'ils sont prêts à collaborer.
       * Communiquer régulièrement avec l'équipe pour faire avancer étape par étape le projet de manière constructive (ateliers de co-conception, explications des découvertes faites pendant les tests utilisateurs).


### FAQ pour les communautés de logiciel libre qui veulent "accueillir" un·e designer



   * **Pourquoi intégrer des designers ?**
       * On fait des logiciels pour des humains, il faut que ce soit utilisable par des humains ;) => on fait du libre pour gagner du contrôle sur nos applications pourquoi cela devrait-il être au dépend du confort ?
       * Si t'as besoin d'en savoir plus, on regroupe dans la section "Retours d'expérience" des partages de communauté qui racontent leur collaboration avec un ou une designer, pourquoi iels ont fait appel à lui et comment ça c'est passé !
   * **Où trouver des designers pour m'aider**
       * Tu peux aller sur Framateam pour partager ton projet, ou envoyer une demande au compte Mastodon pour qu'il la diffuse à ses abonnés =)
   * **Comment faire pour que ça se passe au mieux ?**
       * Expliquer au mieux son projet pour intéresser de potentiels designers (pas trop pour pas les saoûler avant la fin).
       * Expliquer ce qu'on attend d'eux (retravailler une interface / un parcours utilisateur, faire des tests utilisateur, créer une charte graphique...)
   * **J'y comprends rien à tout votre vocabulaire là ! (UX, UI, charte graphique…)**
       * Pas de problème, vient sur Framateam et explique nous ton problème, on saura sans doute te donner les bons mots selon ce dont tu as besoin :)
       * Un article qui pourrait peut-être t'aider : **["Quel designer es-tu ?"](https://www.maiwann.net/blog/quel-designer-es-tu/)**


### FAQ Je ne suis "que" utilisateur·ice de logiciel libre, est-ce que je peux participer ?



Oui ! Tu peux suivre le compte Mastodon Design & Libre ou te faire connaître sur Framateam, on cherche sans doute des personnes pour tester les améliorations du logiciel !

Les différents projets libres ont souvent une section contact, n'hésite pas à leur faire un retour par ce moyen.



## Retours d'expérience : Des exemples de contributions design dans des projets libres



*Des cas concrets d'intervention/participation de designers au sein de projets. (Avant de rédiger du contenu plus structuré, peut-être commencer par des "études de cas"/"retour d'expérience" individuels)*


   * Communauté LibreOffice [https://fr.libreoffice.org/community/design/](https://fr.libreoffice.org/community/design/)
   * La refonte du site des rapports d'Exodus Privacy : [https://framablog.org/2019/11/29/collaborer-pour-un-design-plus-accessible-lexemple-dexodus-privacy/](https://framablog.org/2019/11/29/collaborer-pour-un-design-plus-accessible-lexemple-dexodus-privacy/)
   * UX et logiciels libres : retour d’expérience (TAILS) [http://romy.tetue.net/ux-et-logiciels-libres-retour-d-experience-tails](http://romy.tetue.net/ux-et-logiciels-libres-retour-d-experience-tails)
   * 3 questions à Marie-Cécile Godwin-Paccard de la team Mobilizon [https://framablog.org/2019/07/03/3-questions-a-marie-cecile-godwin-paccard-de-la-teammobilizon/](https://framablog.org/2019/07/03/3-questions-a-marie-cecile-godwin-paccard-de-la-teammobilizon/)


## Des ressources autour du design et du libre



### Les articles qu'on a écrit ou qu'on relaie consciemment

   * **Designers, accueillez-les tous :** [https://framatube.org/videos/watch/ccaed9bc-60ab-455a-baa3-3c9bff33e376](https://framatube.org/videos/watch/ccaed9bc-60ab-455a-baa3-3c9bff33e376)


   * **Designers & Logiciels libres : et si on collaborait ?**
       * Article : [https://www.maiwann.net/blog/designers-\&-logiciels-libres-si-on-collaborait/](https://www.maiwann.net/blog/designers-\&-logiciels-libres-si-on-collaborait/)
       * Conférence : [https://framatube.org/videos/watch/9f8b8104-cb0e-4688-93cd-d66ec9177960](https://framatube.org/videos/watch/9f8b8104-cb0e-4688-93cd-d66ec9177960)


   * **Libre, moche et alors ?** [https://videos.ubuntu-paris.org/videos/watch/0a4af8a1-4825-4b08-8962-4e3a05fcbeb6](https://videos.ubuntu-paris.org/videos/watch/0a4af8a1-4825-4b08-8962-4e3a05fcbeb6)


   * **On a besoin de compétences en UX design dans les projets libres** [https://www.cipherbliss.com/on-a-besoin-de-competences-en-ux-design-dans-les-projets-libres/](https://www.cipherbliss.com/on-a-besoin-de-competences-en-ux-design-dans-les-projets-libres/)

   * [Mémoire/thèse] **Le design des programmes - Des façons de faire du numérique** [http://www.softphd.com](http://www.softphd.com)
   * [Mémoire/thèse] **Pour un design graphique libre** [http://www.etienneozeray.fr/libre-blog/](http://www.etienneozeray.fr/libre-blog/)


   * **Et si on tenait compte des utilisateur·ices dans les projets libres** [https://framablog.org/2018/10/02/et-si-on-tenait-compte-des-utilisateur%C2%B7ices-dans-les-projets-libres/](https://framablog.org/2018/10/02/et-si-on-tenait-compte-des-utilisateur%C2%B7ices-dans-les-projets-libres/)


   * **Changer le monde un égo à la fois, est-ce que choisir le libre c'est choisir le contrôle au dépend du contrôle ?** [https://tube.4aem.com/videos/watch/872799ec-eea8-46a5-b959-e555b9eb1e25](https://tube.4aem.com/videos/watch/872799ec-eea8-46a5-b959-e555b9eb1e25)


   * **Non, les designers du web ne sont pas des artistes** [https://weblog.redisdead.net/main/post/2015/08/28/Non-les-designers-du-web-ne-sont-pas-des-artistes](https://weblog.redisdead.net/main/post/2015/08/28/Non-les-designers-du-web-ne-sont-pas-des-artistes)


   * **Quelques astuces pour gérer vos designers-divas en mode associatif** [https://weblog.redisdead.net/main/post/2017/03/30/Quelques-astuces-pour-gerer-vos-designers-divas-en-mode-associatif](https://weblog.redisdead.net/main/post/2017/03/30/Quelques-astuces-pour-gerer-vos-designers-divas-en-mode-associatif)


   * **Dossier de la CNIL "La forme des choix"** [https://linc.cnil.fr/fr/cahier-ip6-la-forme-des-choix-0](https://linc.cnil.fr/fr/cahier-ip6-la-forme-des-choix-0)


### Les articles qu'on relaie mais qu'on a lu il y a longtemps



   * **Le design dans le libre : pistes de réflexion :** [https://mariejulien.com/post/2017/02/08/Le-design-dans-le-libre-%3A-pistes-de-r%C3%A9flexion](https://mariejulien.com/post/2017/02/08/Le-design-dans-le-libre-%3A-pistes-de-r%C3%A9flexion)
   * **Culture hacker et peur du WSIWYG** [http://www.revue-backoffice.com/numeros/01-faire-avec/eric-schrijver-culture-hacker-peur-wysiwyg](http://www.revue-backoffice.com/numeros/01-faire-avec/eric-schrijver-culture-hacker-peur-wysiwyg)


## Des logiciels libres pour les designers



   * presentator.io pour partager des maquettes en png et faire un petit prototype cliquable
   * Pencil project pour créer des maquettes et les exporter
   * usertest.gelez.xyz/ pour faire ses tests utilisateurs à distance
   * Inkscape
   * Gimp, Krita
   * Akira UX (en développement)
   * Scribus


## D'autres endroits où regarder

   * Autres structures existantes
       * [https://opensourcedesign.net](https://opensourcedesign.net)
       * [https://opensource.com/](https://opensource.com/)
       * [https://opensource.guide/](https://opensource.guide/)
   * Copains copines ?
       * [https://www.ethicsfordesign.com](https://www.ethicsfordesign.com)
       * [https://designersethiques.org/](https://designersethiques.org/)
       * [https://lowtechlab.org/](https://lowtechlab.org/)


## Où se croiser/rencontrer



   * Événement lié au libre [Période/Où]
       * [https://www.agendadulibre.org/](https://www.agendadulibre.org/)
       * [https://capitoledulibre.org/](https://capitoledulibre.org/) [Toulouse / novembre]
       * [https://www.jdll.org/](https://www.jdll.org/) [ Lyon / avril]
       * [https://passageenseine.fr/](https://passageenseine.fr/) [Choisy-le-Roi / début juillet]
       * Ubuntu Party [http://ubuntu-paris.org/](http://ubuntu-paris.org/)
       * Fosdem [Bruxelle / ]
   * Évènement lié au design
       *

   * Aux deux
       * [http://outilslibresalternatifs.org/](http://outilslibresalternatifs.org/)
       * [https://libregraphicsmeeting.org/](https://libregraphicsmeeting.org/) [Rennes / mai]


## Qui a écrit cette page ?



*Mettez vos noms ! :)*


   * Florian Bourmaud ([https://florianbourmaud.com/)](https://florianbourmaud.com/))
   * Maïtané Lenoir alias Maiwann ([https://www.maiwann.net)](https://www.maiwann.net))
   * Corentin Brulé @macrico